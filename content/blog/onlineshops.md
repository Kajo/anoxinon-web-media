+++
title = "Datenschutz beim Onlineshopping"
date = "2019-12-11T14:00:00+01:00"
tags = ["Anfänger"]
categories = ["Datenschutz"]
banner = "/img/thumbnail/onlineshopping.png"
description = "E-Commerce ist extrem lukrativ. Der Begriff umfasst nicht nur den alleinigen elektronischen Warenhandel und -versand sondern vielmehr auch die technisch gestützte Antizipation von Kundenwünschen durch die am Verkauf ihrer Waren interessierten Unternehmen. Wie E-Commerce im Detail funktioniert und worauf man als Kunde achten sollte klären wir im folgenden Artikel. "

+++
> Trotz sorgfältiger Recherche behalten wir uns Irrtümer vor. Dies ist keine Rechtsberatung. Bei juristischen Fragen wenden Sie sich bitte an einen fachkundigen Anwalt.

E-Commerce ist extrem lukrativ. Das ist weder verwunderlich noch von der Hand zu weisen, schließlich sprechen die Zahlen für sich: [14,1%](https://www.statista.com/statistics/534123/e-commerce-share-of-retail-sales-worldwide/) des weltweiten Einzelhandelsumsatzes werden 2019 im E-Commerce erlöst, das entspricht unvorstellbaren 3,5 Billiarden US-Dollar oder annähernd dem Bruttoinlandsprodukt der Bundesrepublik Deutschland.



---

**Inhaltsverzeichnis:**  

1. [Einleitung](#einleitung)  
2. [Bestandsaufnahme](#bestand)  
	2.1 [Customer obsession = Datenhunger](#customer)  
   	2.2 [Vollzugsdefizit DSGVO](#dsgvo)  
   	2.3 [Das Batman-Prinzip oder: schlimmer geht immer](#batman)  
	
3. [Datenschutzbewusst einkaufen:](#datenschutzbewusst)  
	3.1 [ohne Drittanbieter-Tracking](#trackingfrei)  
	3.2 [auf datenschutzfreundlichen Plattformen](#datenschutzfreundliche_plattformen)  
4. [Schlussworte](#schlussworte)  

---

### 1. Einleitung{#einleitung}

Was über diese Zahlenakrobatik oft vergessen wird: die Profitabilität von E-Commerce ist nicht entscheidend in Geld messbar, sondern speist sich auch und gerade aus Daten; Daten, die Online-Shops als Werbe- und Verkaufsplattformen einem Kunden mit bemerkenswerter Effizienz abgreifen. Auskunftsverlangen bei Amazon (zum Beispiel [hier](https://www.spiegel.de/netzwelt/web/amazon-experiment-was-der-konzern-mit-jedem-klick-erfaehrt-a-1205079.html)) haben bei beharrlichem Warten gezeigt, dass nicht etwa nur Bestellvorgänge protokolliert werden. Weit gefehlt: bei jedem einzelnen Klick des Kunden sind [Tracker](https://www.privacy-handbuch.de/handbuch_13.htm) am Werk und legen Datensätze (zu IP, System, Sprachpräferenz, Endgerät u.v.m.) an, die sich zu einem umfangreichen Interessen-, Alltags- und sogar Bewegungsprofil zusammenfügen lassen - und das ganz ohne den Einsatz von „Heimassistenten“ wie Amazon Echo.

Diese Einsicht sollten an niemandem spurlos vorübergehen, auch nicht an passionierten Online-Shoppern. Daher möchte dieser Beitrag Aufklärungsarbeit in Sachen Datenschutz leisten, um nicht vorschnell der eigenen Bequemlichkeit im Kaufrausch, vor allem in der Vorweihnachtszeit, zu erliegen. Es geht dabei nicht um die Dämonisierung von Amazon, Ebay & Co, sondern um folgende simple Fragestellung: auf was lässt man sich mit einem vermeintlich so lapidaren One Click ein? Und gibt es Alternativen, dass eigene Shoppingerlebnis im Online-Handel datenschutzfreundlicher zu gestalten?

### 2. Bestandsaufnahme{#bestand}

Was hier am *click monitoring* veranschaulicht wurde, ist Teil einer ganzen Reihe von datenschutzrechtlichen Bedenken gegen populäre E-Commerce-Plattformen. Sie sollen im Folgenden am Beispiel der marktbeherrschenden [„Big Three“ im deutschen Online-Shopping](https://de.statista.com/statistik/daten/studie/170530/umfrage/umsatz-der-groessten-online-shops-in-deutschland/) – Zalando, Otto und mit weitem Abstand voran natürlich Amazon – aufgezeigt werden.  

#### 2.1 Customer obsession = Datenhunger{#customer}

Amazon nennt es „[customer obsession](http://www.game-changer.net/2018/06/28/what-is-customer-obsession/#.Xclda25Fyno)“ (in etwa: Kundenvernarrtheit), Zalando „[customer focus](https://corporate.zalando.com/de/verantwortung/unsere-unternehmenskultur)“, bei Otto ist etwas schwammig vom kundenorientierten „[Kulturwandel 4.0](https://www.ottogroup.com/de/karriere/Unternehmenskultur/Kulturwandel-4.0.php)“ die Rede. Gemeint ist letztlich dasselbe: bislang unausgesprochene Kundenwünsche mit technischen Mitteln zu antizipieren, zu implementieren und daraus Kapital zu erwirtschaften. Dass für solche Verhaltensprognosen die Nutzerdaten eine wahre Goldgrube sind, wird einem schnell bewusst.

Ein Blick in die Datenschutzerklärungen der E-Commerce-Riesen schafft weitere Klarheit. In mehr oder weniger ausgeprägter Transparenz schlüsseln Amazon, Zalando und Otto dort auf, welche Daten erhoben werden.

[Amazon ](https://www.amazon.de/gp/help/customer/display.html?nodeId=3312401) unterscheidet in seiner Datenschutzerklärung drei Kategorien von Daten nach ihrer Herkunft und verlinkt Beispiele.

a. *Informationen mit Opt-Out-Möglichkeit* 

Alles, was der Nutzer freiverantwortlich über sich bekannt gibt und nach Art. 7 Abs. 3 DSGVO jederzeit widerrufen kann. Am sensibelsten dürften dabei Telefonnummern (zur an sich sinnvollen [Zwei-Faktor-Authentifizierung](https://mrdatenschutz.de/2019/10/online-account-schutzen-zwei-faktor-authentifizierung-2fa-bei-amazon-de/)), Identitätsdokumente (zum Nachweis der Volljährigkeit), WLAN-Logins und nicht zuletzt Kontaktlisten sein. Hier hat der Nutzer Wahlfreiheit zwischen Privatsphäre und der Ausschöpfung von Amazons Produktpalette, was allerdings sehr zurückhaltend ausgeübt werden sollte. Anders ausgedrückt: nicht Amazon (sondern idealerweise ein Open Source-Dienst wie [Nextcloud](https://nextcloud.com/)) sollte zum Cloud-Dienstleister werden, wenn man z.B. Fotos aus dem letzten Strandurlaub mit Freunden teilen will. Anderenfalls läuft man letztlich Gefahr, von individuell zugeschnittener Werbung (sog. *tailored advertising)* mit Bademode und Sonnencreme überrollt zu werden.

b. *Automatisch gesammelte Informationen*

Hierher gehört das bereits erwähnte *click monitoring,* was auch den Seitenverlauf (sog. *Clickstream*) zu und von Amazon-Seiten beinhaltet. Verarbeitungszweck ist Amazon zufolge u.a. die Betrugsprävention, was unter Art. 6 Abs. 1  S. 1 Buchstabe e und f DSGVO fiele. Dass sich Tracker, wie Amazon Service Metrics darauf beschränken und nicht vorrangig Werbezwecken dienen, ist aber wenig glaubhaft.

<img src="/img/onlineshops/amazon.png" class="fullsize" />  

Offenkundig geht es etwa nicht um Missbrauchsbekämpfung, wenn schädliche Adware wie lijit.com [Cookies](https://anoxinon.media/anxicon/#c) sammelt oder dies zumindest toleriert wird. Auch nicht hilfreich ist es, wenn Auskunftsersuchen nach Art. 15 DSGVO zu den automatisiert abgegriffenen Daten ihrerseits automatisiert und unvollständig beantwortet wurden. Dies stellte jedenfalls die gemeinnützige Datenschutzorganisation [noyb (Max Schrems)](https://noyb.eu/streaming_auskunft/?lang=de) zu Jahresbeginn bei Amazon Prime und weiteren Streaming-Diensten fest. Insbesondere die Zwecke der Datenverarbeitung wurden nicht hinreichend kommuniziert.

c. *Informationen aus anderen Quellen*

In erster Linie betritt Amazon hier das datenschutzrechtliche Minenfeld externer Zahlungsdienstleister. Zum einen durch Bonitätsabfragen bei der SCHUFA oder anderen Kreditauskunfteien, deren systemische Probleme (unklare Berechnungsgrundlage der Scores, Falschzuordnung) wir bereits in einem [früheren Artikel](https://anoxinon.media/blog/datenschutzindervermietung/#SCHUFA) erläutert haben.

Zum anderen erhält Amazon Zahlungsinformationen von Unternehmen, mit denen Werbepartnerschaften (sog. „*co-branded*“-Angebote) unterhalten werden. Das betrifft zum Beispiel in Kooperation mit Visa angebotene Kreditkarten, die spezielle Kundenrabatte verheißen. Die Datenschutzerklärung beeilt sich in puncto Datensicherheit zu versichern: „*Beim Umgang mit Kreditkarten befolgen wir den Payment Card Industry Data Security Standard (PCI DSS)*.“ Die PCI DSS Compliance besagt allerdings nichts zur konzerninternen Datenintegrität.

Überhaupt lassen die vagen Angaben zum Datenaustausch mit Dritten auch Rechtsverstöße zu. Die wacklige Grundlage von Art. 6  Abs. 1  S. 1 Buchstabe b DSGVO, eine Notwendigkeit zur Vertragserfüllung, darf man jedenfalls bei der Datenweitergabe zur *„Analyse unserer Datenbanken und der Unterstützung* *bei Werbemaßnahmen“* durchaus anzweifeln – die Paketzustellung wird nicht durch diese Zusatzdienste ermöglicht. Insgesamt gilt: Amazon ist für sog. Auftragsverarbeitung durch Dritte nach Art. 24, 82 DSGVO ggf. verantwortlich.

[Otto ](https://www.otto.de/datenschutz/) gestaltet seine Datenschutzerklärung deutlich konkreter und listet die beteiligten Unternehmen und Tools im Einzelnen auf. Positiv fällt die Informationsarbeit unter *3.4.1. Cookies – Allgemeine Informationen* und *5. Ihre Rechte* (insbesondere Widerspruchsrecht, Art. 21 DSGVO) auf. Auch wird im Regelfall die einschlägige Rechtsgrundlage innerhalb des Art. 6 DSGVO ausgewiesen.

Das Blockieren von Cookies führte freilich im Selbsttest über einen konventionellen Browser (bspw. Google Chrome) dazu, dass Artikel gar nicht erst im Warenkorb abgelegt werden konnten. Damit ist der Online-Shop natürlich nicht mehr wirklich benutzbar.

<img src="/img/onlineshops/otto.png" class="fullsize" />  

Somit stellen sich im Ergebnis ähnliche Probleme wie bei Amazon – wenn auch in abgeschwächter Form. Das vom Dienst mit dem ironisch klingenden m-pathy (*3.4.9*.) durchgeführte *click monitoring* kommt ebenso vor wie die Überwachung des Clickstreams durch Affilinet (*3.4.11*). Wie effektiv die Widerspruchsmöglichkeit nach Art. 21 DSGVO sich in diesen Bereichen bisher gegenüber Otto erwiesen hat, konnten wir nicht nachprüfen.

[Zalando ](https://www.zalando.de/zalando-datenschutz/) stellt schließlich eine sehr unübersichtliche Datenschutzerklärung zur Verfügung. Die Sortierung nach grundlegenden und „Profi“-Informationen mit zahlreichen ausklappbaren Unterkapiteln konterkariert eine Kernaufgabe von Datenschutz, nämlich: jedermann eine schnelle und vollständige Einsicht in die Datenverarbeitung und ihre Zwecke zu ermöglichen. Die Verarbeitungszwecke selbst sind schwammig formuliert (z.B. *2.7 Geschäftssteuerung und Geschäftsoptimierung),* auch der permanent zitierte Auffangtatbestand des Art. 6 Abs. 1 S. 1 Buchstabe f DSGVO (berechtigtes Interesse an der Verarbeitung) gilt nicht schrankenlos. Vielmehr ist im Einzelfall eine Abwägung mit dem Grundrecht auf informationelle Selbstbestimmung (Art. 2 Abs. 1, Art. 1 Abs. 1 GG) erforderlich.

Die cookie- und trackerfreie Nutzung war auch bei Zalando kaum möglich, schon die Schaltfläche „*In den Warenkorb*“ konnte nicht angesteuert werden.

#### 2.2 Vollzugsdefizit DSGVO{#dsgvo}

Angesichts der dargestellten Bedenken sollte man meinen, dass Datenschutzbehörden schon in größerem Stil auf die Sanktionsmittel der DSGVO gegen E-Commerce-Plattformen zurückgegriffen haben. Der Bußgeldrahmen des Art. 83 Abs. 5 DSGVO – bis zu 20 Mio. € oder 4% des Jahresumsatzes – ist bis dato allerdings nicht ansatzweise ausgeschöpft worden. Soweit ersichtlich wurde im September 2019 gegen den Pizzalieferdienst Delivery Hero, der [in Höhe von 80.000 €](https://netzpolitik.org/2019/berliner-datenschutzbehoerde-verhaengt-bisher-hoechstes-dsgvo-bussgeld-gegen-lieferdienst/) zur Kasse gebeten wurde, die bislang höchste Geldbuße verhängt.

Es bleibt zu hoffen, dass künftig auf Betreiben von Organisationen wie *noyb* der Sanktionsgedanke auch den Riesen des Online-Einzelhandels gegenüber greift, wenn sie die Bestimmungen der DSGVO unterlaufen.

#### 2.3 Das Batman-Prinzip oder: schlimmer geht immer{#batman}

Blickt man über den Geltungsbereich der DSGVO hinaus, ist die Besorgnis nach Datensicherheit und -integrität ungleich höher. Die kollektiv als [BAT](https://xpress.db.com/2018-juli/x-pect/baidu-alibaba-und-tencent-in-einem-produkt/) bezeichneten E-Commerce-Giganten [Baidu](https://de.wikipedia.org/wiki/Baidu), [Alibaba](https://de.wikipedia.org/wiki/Alibaba_Group) und [Tencent](https://de.wikipedia.org/wiki/Tencent) sind ebenso keine Unternehmen die dem Schutz der Privatsphäre ihrer Kunden einen hohen Stellenwert zuschreiben . Während im Verhältnis zu den USA das, vom Datenschutzniveau her betrachtet weit hinter der DSGVO zurückbleibende, [Privacy Shield](https://de.wikipedia.org/wiki/EU-US_Privacy_Shield) Abkommen gilt, entzieht sich etwa Alibaba weitgehend datenschutzrechtlicher Regulierung. Das Unternehmen hat mit seinem erfolgreichen Bewertungssystem [Sesame Credit](https://www.zeit.de/wirtschaft/2018-05/alibaba-hema-supermaerkte-datenschutz-nutzerdaten/seite-3) die staatlichen Pläne der Kommunistischen Partei Chinas nach einem Social-Scoring vorweggenommen und ist in der Lage, in fast schon dystopischem Ausmaß das Käuferverhalten zu steuern.

### 3. Datenschutzbewusst einkaufen:{#datenschutzbewusst}

Angesichts des bestehenden Umgangs mit Nutzerdaten im E-Commerce ist Anlass genug, ihm positive Beispiele gegenüber zu stellen. Im ersten Schritt ist aufzuzeigen, wie man ohne besonderes technisches Know-how ,trotz Beibehaltung von Amazon & Co., sparsamer mit den eigenen Daten umgehen kann. Im zweiten Schritt sind - darüber hinaus gehend - einige Online-Shops aufgeführt, die dem Datenschutz in besonderem Maße Rechnung tragen.

#### 3.1 ohne Drittanbieter-Tracking{#trackingfrei}

Vorab ist zu erwähnen, dass es nicht sehr schwer und zeitaufwändig ist, den Einfluss der allgegenwärtigen Trackern zu minimieren. Ganz intuitiv lässt sich zum Beispiel in, den am weitesten verbreiteten Browsern, [Firefox](https://addons.mozilla.org/de/firefox/addon/umatrix/) und [Chrome](https://chrome.google.com/webstore/detail/umatrix/ogfcmafjalglgifnmanfmnieipoejdcf?hl=de) die Erweiterung *uMatrix* installieren. Das Add-On blockiert automatisch Cookies mit Werbebezug und hindert eine Weitergabe des Clickstreams. Illustriert wird das erlaubte Trackingverhalten mit einem leicht verständlichen Rot-Grün-Schema, welches nach Bedarf manuell angepasst (d.h. insbesondere verschärft) werden kann. Für häufig besuchte Webseiten lohnt die Erstellung von reproduzierbaren Regeln für *uMatrix.*

<img src="/img/onlineshops/umatrix.png" class="fullsize" />  

Eine recht ausführliche Anleitung, die *uMatrix* auf Firefox erklärt, ist [hier](https://www.kuketz-blog.de/firefox-umatrix-firefox-kompendium-teil9/) zu finden.

Wer bereit ist, sich vom liebgewonnenen konventionellen Browser zu trennen, kann außerdem auf den Firefox nahen [Tor Browser](https://www.torproject.org/download/) umsteigen. Auch dieser sagt Trackern mit dem vorinstallierten und intuitiv bedienbaren Tool *NoScript* den Kampf an (vgl. oben den ersten Screenshot zu Amazon). Zusätzlich findet eine Anonymisierung der standortbasierten IP-Adresse durch Umleitung über drei Server (sog. *relays)* des Tor-Netzwerks statt.

<img src="/img/onlineshops/tor.png" class="fullsize" />  

#### 3.2 auf datenschutzfreundlichen Plattformen{#datenschutzfreundliche_plattformen}

Eine gleichwertige Alternative zu den vorgestellten E-Commerce-Riesen gibt es nicht, was bei ökonomischer Betrachtung in Sachen Sortiment, Händlervielfalt und Preisgestaltung ohnehin illusorisch scheint. Nachstehend jedoch einige Empfehlungen, die sich positiv von der Datenhungrigkeit von Amazon & Co. abheben:

\- [buch7.de](https://buch7.de) (Bücher)

\- [floss-shop.de](https://www.floss-shop.de/de/>) (Elektronik, OpenSource)

\- [varia-store.com](https://www.varia-store.com>) (Elektronik)

\- [fairmondo.de](https://www.fairmondo.de/) - eine Plattform welche nach dem Genossenschaftsprinzip operiert und die Möglichkeit eines Kleinanzeigen Marktes bietet

### 4. Schlussworte{#schlussworte}

Das jährliche Weihnachtsgeschäft ist längst angebrochen und damit auch für den E-Commerce die Umsatz stärkste Zeit des Jahres. Wenn die Kreditkarte dann glüht und der Rubel rollt, muss nicht zugleich Hochbetrieb bei systematischer Datenverarbeitung durch die bekannten Online-Shops herrschen. Vielmehr hoffen wir mit dem vorstehenden Beitrag gezeigt zu haben, dass Abhilfe notwendig und ohne großen Aufwand möglich ist. So kann dem ungeregelten Datenfluss im Einklang mit der DSGVO entgegengewirkt und eine Nachhaltigkeit im eigenen Datenprofil sichergestellt werden. Diese Entscheidung für informationelle Selbstbestimmung, für Autonomie und Freiräume in der Informationsgesellschaft ist doch ganz im Geist weihnachtlicher Besinnung.
