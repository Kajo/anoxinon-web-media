+++
title = "Die Macht der Datensammler"
date = "2019-01-19T10:00:00+01:00"
tags = ["Anfänger"]
categories = ["Datenschutz", "Smart Home"]
banner = "/img/thumbnail/macht_der_datenunternehmen_thumbnail.png"
description = "Wie wichtig der Schutz von Daten für ein Unternehmen ist, kann man bereits an der Datenschutzerklärung erkennen. Trotz dem Versprechen für den Schutz der Daten zu sorgen, handeln viele eher gegenteilig und treten den Nutzer mit den Füßen. In diesem Beitrag möchten wir einige Denkanstöße bieten."
+++

> Dieser Beitrag ist für Anfänger geeignet

### I. Skandale? Bitte was?

Datenschutz kann wie ein abstraktes Konzept wirken. Weshalb ist es so wichtig, dass ich mich als einzelner gegen die Datensammelwut von großen Anbietern wehre? Worin besteht die "Macht der Daten"?

Leider wird diese Macht oft unterschätzt. Dass Facebook massenhaft Menschen nicht nur unterschwellig beeinflusst, ist inzwischen ausreichend belegt. Es gab zahlreiche Schlagzeilen zur Wahlmanipulation im US-Wahlkampf oder zu Cambridge Analytica. Eine kurze Übersicht über die Datenskandale bei Facebook im Jahre 2018 findet sich [hier](https://netzpolitik.org/2018/die-ultimative-liste-so-viele-datenskandale-gab-es-2018-bei-facebook/ "Artikel bei Netzpolitik.org").
Wer etwas tiefer in die Materie einsteigen möchte: [Ein Worst-Of Facebook (englisch)](https://np.reddit.com/r/AntiFacebook/wiki/timeline).

Es wurde [unter anderem enthüllt](https://www.zeit.de/digital/internet/2014-06/facebook-nutzer-manipulation-studie), dass für eine Facebook-interne Studie die Gefühle von Menschen beeinflusst werden sollten. Prekär war unter anderem, dass Teenager, die mit sich selbst unzufrieden waren, dafür ausgewählt wurden, zielgerichtete Werbung - u.a. für Kosmetika - zu sehen.

Viele Menschen schimpfen über Facebook, ziehen aber aus den Datenskandalen kaum Konsequenzen - beispielsweise gehören die neutral wirkenden "Alternativen" WhatsApp und Instagram auch zum Facebook-Konzern. In Instagram wird bereits Werbung anzeigt, bei Whatsapp folgt dies im Laufe des Jahres.
![Übersicht über den Fa. Konzern](/img/machtderdatenunternehmen/fa.png)

Mittlerweile nutzen über 90% aller 12-19-Jährigen [täglich oder mehrmals die Woche Whatsapp](https://de.statista.com/infografik/11930/wie-jugendliche-am-liebsten-kommunizieren/) und 67% Instagram.


### II. Die Moral der Geschichte

Man sollte sich die generelle Frage stellen, ob es moralisch in Ordnung ist, Menschen mittels gewonnenen Erkenntnissen zu manipulieren.

Auch wenn die meisten Nutzer mit einer gesunden Reflexion erkennen können, dass die Werbung an sie angepasst ist und an sie appelliert, gibt es auch solche, die dies nicht können. Darunter fallen Kinder und Jugendliche sowie Menschen, die - egal aus welchem Grund - nicht in der Lage sind, diese "Masche" zu durchschauen.

Ist es moralisch vertretbar, einem Sportler das neuste Hantelset anzubieten?

Warum sollte man aus Sicht eines Werbetreibenden einer Person mit Minderwertigkeitskomplexen keine Kosmetika verkaufen?

Wo zieht man die Grenze?  
Ist es in Ordnung, einer Krebspatientin als Heilung formschöne, energetisierende Zimmerbrunnen anzubieten? Für Außenstehende mag das skurril klingen, jedoch gibt es Lebenssituationen, in denen Menschen jede Hoffnung ergreifen die sich ihnen bietet.

### III. Die einfache Wahl

In der heutigen Zeit werden Daten immer wertvoller. Die Software ist meist so ausgelegt, dass eine Zustimmung zur Datensammlung einfacher ist und hübscher aussieht.  
Bei Windows sind zum Beispiel die Privatsphärereneinstellungen kompliziert, verstreut und nicht ausreichend wirkungsvoll. Bei einem Smartphone wird man dazu gedrängt oder gar gezwungen, ein Konto bei dem Hersteller anzulegen.

Der Nutzer wird dazu bewegt, den offensichtlicheren Weg einzuschlagen. Übermäßig viel Text bei den allgemeinen Geschäftsbedingungen oder bei der Inbetriebnahme eines Technikprodukts verleiten Nutzer zu einer schnellen, nicht durchdachten Zustimmung.

Wie weit dies gehen kann, hat 2017 der [WLAN-Anbieter Purple](https://www.heise.de/newsticker/meldung/22-000-Hotspot-Nutzer-willigen-ins-Toilettenputzen-ein-3771983.html) demonstriert. In den AGB verpflichtete dieser 22.000 Kunden, 1.000 Stunden gemeinnützige Arbeit zu leisten. Darunter wurde auch das Putzen von Toiletten auf Festivals, das Abkratzen von alten Kaugummis vom Gehweg oder das Reinigen von Abwasserrohren mit der Hand aufgeführt.


### IV. Smarte neue Welt

Die "Datensammelwut" der Großkonzerne stoppt jedoch nicht vor der heimischen Fußmatte. Google zieht zum Beispiel mit Google Home und Amazon mit Echo in den Haushalt ein. Viele Menschen verlassen sich darauf, dass der Datenschutz bei diesen Produkten gewährleistet ist.

![Übersicht über den Go. Konzern](/img/machtderdatenunternehmen/go.png) ![Übersicht über den Am. Konzern](/img/machtderdatenunternehmen/am.png)


Bedenken gibt es jedoch bei allen "smarten" Geräten, die sich mit dem Internet verbinden um ihren Dienst zu erfüllen. Sie bilden das "Internet der Dinge" - oder "Internet of Things" (IoT).

Hier eine kleine Auswahl:

• Einem Android-Smartphone ist eine eindeutige Tracking-ID zugeordnet. Sobald das Gerät einschaltet wird, kann Google damit Persönlichkeitsprofile erstellen. Abhilfe schaffen nur umfangreiche System-Modifikationen oder die Nutzung einer alternativen Android-Distribution (z.B. LineageOS). *Mehr dazu erfahren Sie in einem folgenden Beitrag.*

• Die meisten Nutzer von Smartwatches oder Fitnesstrackern benutzen diese entweder, um die Leistung ihres Körpers zu optimieren, Benachrichtigungen zu erhalten oder ihr Schlafverhalten zu erfassen. Leider sind die zugehörigen Apps meist so programmiert, dass vieles in der Cloud des Anbieters landet. Oft ist es dem Nutzer nicht möglich alternative Apps zu nutzen, da die Smartwatch ohne die offizielle App schlicht den Dienst verweigert. Prekär ist unter anderem, dass sich der Hersteller umfassende Rechte in seinen AGB und der Datenschutzerklärung einholt. Dazu gehört unter anderem die Weitergabe von Daten an Dritte. In welchem Umfang dies geschieht, ist vom Anbieter abhängig.

• Auch moderne [Kühlschränke](https://tcf.org/content/commentary/new-fridge-spying/), Glühbirnen und manche [Staubsauger-Roboter](https://www.nytimes.com/2017/07/25/technology/roomba-irobot-data-privacy.html) können eine Verbindung zum Netz herstellen und Daten versenden.

• Die Idee von "smarten" Geräten wird bis zur Absurdität getrieben. Das Crowdfunding-Projekt Juicero entwickelte einen "Entsafter", das Produkt war jedoch nur in der Lage, einen Plastikbeutel des Herstellers zu zerdrücken, in dem die bereits verarbeiteten, konservierten Früchte verpackt waren. Damit jedoch nicht genug: Jeder Plastikbeutel hatte einen QR-Code, und das Gerät musste mit dem Internet verbunden werden, um zu kontrollieren, dass es auch ein "echter" Beutel des Herstellers war. Außerdem sammelte die zugehörige App, die ebenfalls nötig war, um die Presse zu nutzen, jede Menge Daten.

Zur allgemeinen Erheiterung tragen ebenfalls Projekte bei, deren Sinn sich den meisten Nutzern nicht erschließt, wie smarte Schuhe oder ein smarter Salzstreuer mit Lautsprecher und Stimmungslicht.

Das Attribut "smart" bedeutet oft einfach nur, dass sich das Gerät mit dem Internet verbindet. Meist ist dies zur Funktion sogar unabdingbar.

Die wichtigste Frage, die man sich bei solchen Geräten stellen sollte ist, ob dies wirklich nötig ist. Braucht meine Glühbirne eine Verbindung zu einem Server in den USA, um darüber mit dem Lichtschalter zu kommunizieren?

Es gibt jedoch nicht nur Datenschutz-, sondern auch Sicherheitsprobleme. Jedes "smarte" Gerät ist ein potentielles [Einfallstor in das heimische Netzwerk](https://www.heise.de/newsticker/meldung/35C3-Ueber-die-smarte-Gluehbirne-das-Heimnetzwerk-hacken-4259891.html). Es gibt sogar bereits [Suchmaschinen](https://shodan.io/) für das Internet der Dinge. Mit diesen Werkzeugen kann man ganz einfach IoT-Geräte (mit den zugehörigen öffentlichen IP-Adressen) suchen.

Der Markt wird aktuell mit solchen Geräten geflutet, das schließt unter anderem Billig-Router, Überwachungskameras, smarte Glühbirnen und Haushaltsgeräte ein. Leider hat für viele Hersteller die Sicherheit ihrer Produkte keine hohe Priorität - im Gegenteil. Oft werden sogar in der Software vorhandene Sicherheitsmaßnahmen einfach [nicht genutzt](https://www.heise.de/security/meldung/Software-auf-vielen-Routern-nutzt-keine-etablierten-Sicherheitsmechanismen-4268046.html).

### V. Schlussworte

Auch wenn viele CEOs - wie zum Beispiel Tim Cook (Apple) - betonen, wie wichtig ihnen Datenschutz und -sicherheit sind, bieten proprietären Geräte und deren Software keinerlei Möglichkeit, diese Versprechen zu kontrollieren. Eine Niederlassung in Irland, die unter europäisches Datenschutzrecht fällt, bietet an sich keinen ausreichenden Datenschutz.

"Vertrauen ist gut, Kontrolle ist besser" sollte unserer Ansicht nach der Standard im Bereich der persönlichen Daten sein. Anstatt sich auf Werbeversprechen zu verlassen, wäre es sinnvoller quelloffene Lösungen zu nutzen, die am besten noch unabhängigen Prüfungen ("Audits") unterzogen wurden.

Bei der Wahl des Betriebssystems für den Desktop-Rechner und den Laptop sollte möglichst ein freies System bevorzugt werden. Auch beim Smartphone ist es eine Überlegung wert, bspw. eine alternative Android-Distribution wie LineageOS zu nutzen, die ohne Google-Apps und -Systemdiensten ausgeliefert wird.

Im Alltag ist jedoch die Wahl der genutzen Software und Dienste mit am wichtigsten. Messenger sollten z.B. nicht primär nach Verbreitung oder Aussehen gewählt werden.

Wir werden in Zukunft genauer auf solche Thematiken eingehen, bleiben Sie also weiterhin gespannt :)
