+++
title = "Linux für Einsteiger - Teil 1"
date = "2019-02-17T14:00:00+02:00"
tags = ["Anfänger"]
categories = ["Linux", "Freie Software"]
banner = "/img/thumbnail/Tux_Schultute.png"
description = "Linux, was ist das? In dieser Artikelserie möchten wir interessierte Personen an das Thema Linux heranführen. Den Start macht dieser Beitrag über gute Gründe wieso man Linux nutzen sollte."
+++
> Dieser Beitrag ist für Anfänger geeignet

### I. Was ist Linux? Woher kommt es? ###

Linux ist ein kostenloses und frei verfügbares Betriebssystem, welches ein Gemeinschaftsprojekt von Freiwilligen, Non Profit Organisationen (NPO), sowie Unternehmen ist. Es teilt sich in sogenannte Distributionen auf, dazu gleich noch mehr. Es ist eine Zusammensetzung aus dem Linux Kernel (Linux Torvalds, 1991) und Software des GNU Projekts (Richard Stallmann, 1984). Seitdem sind diese beiden Teile stark ineinander gewachsen.

Das Betriebssystem bildet, gemeinsam mit den notwendigen Hardware Komponenten, die Basis eines Computer. Es steuert unter anderem die Kommunikation zwischen Software und Hardware. Im Gegensatz zu anderen Betriebssystemen wie Mac OS oder Windows 10, steht hinter Linux nicht ein einzelnes großes Unternehmen.

### II. Gibt es nur das eine Linux? Unterschiede? ###

Wer nach Linux sucht wird schnell feststellen, dass es nicht die eine ultimative Version gibt. Viel mehr ist Linux als ein Überbegriff für verschiedene Distributionen zu sehen. Einige sind für Unternehmen oder spezielle Zielgruppen aus der Wissenschaft gedacht, andere wiederrum für den "Eigenbau" oder die Heimanwendung. Eine Einschränkung bei der Verwendung existiert jedoch nicht.

Es gibt also nicht das eine System, sondern mehrere und Sie können aus einem größeren Fundus wählen. Die Distributionen unterscheiden sich im Anwendungszweck, sowie der verwendeten Paketverwaltung und der Dateistruktur/Funktionsweise. Desweiteren gibt es auch Unterschiede beim Update Zyklus und den standardmäßig enthaltenen Softwarepaketen.

### III. Gute Gründe für Linux ###

Wir möchten nachfolgend einige Punkte auflisten die für Linux sprechen.

<img src="/img/dsflyer/punkt1.png" align="middle" height="40" width="40">
<b>Kostenlos: </b>Sie sparen viel Geld. Die Distributionen sind meist kostenlos.<br>
<img src="/img/dsflyer/punkt2.png" align="middle" height="40" width="40">
<b>Einfache Bedienung:</b> Die Oberfläche ist einfach und an Ihnen bekannte Systeme angelegt, sodass Linux inzwischen nicht mehr nur für Nerds geeignet ist, wie es noch in den 90er Jahren der Fall war. Sie können aus unterschiedlichen Oberflächen auswählen und sehr vieles individuell anpassen.<br>
<img src="/img/dsflyer/punkt3.png" align="middle" height="40" width="40">
 <b> Nachhaltig:</b> Linux läuft auch hervorragend auf älteren Geräten. Damit können Sie alter Hardware neues Leben einhauchen und diese so weiter nutzen.<br>
<img src="/img/dsflyer/punkt4.png" align="middle" height="40" width="40">
<b>Langzeitsupport:</b> Sie müssen sich die nächsten Jahre keine Sorgen um die Installation eines neuen Upgrades machen. Sie bekommen, je nach gewählter Version, bis zu 5 oder 10 Jahre Community Support.<br>
<img src="/img/dsflyer/punkt5.png" align="middle" height="40" width="40">
<b>Ein Satz und alles ist aktuell:</b>  Ein Paketmanager kümmert sich darum, dass Ihre Software bzw. Ihr System aktuell ist. Im Gegensatz zu anderen Betriebssystemen, wo jedes Programm einzeln zu aktualisieren ist, übernimmt der Paketmanager diese Aufgabe auf Knopfdruck.<br>
<img src="/img/dsflyer/punkt6.png" align="middle" height="40" width="40">
<b>Flexibel:</b> E-Mails schreiben, im Internet suchen und vieles mehr ist kein Problem. Leistungsstarke Office- und Grafikanwendungen stehen kostenlos zur Verfügung. Eine Auflistung an nützlicher Software, werden wir in einem anderen Beitrag zur Verfügung stellen. Auch nicht-quelloffene Software (properitär) funktioniert unter Linux.<br>
<img src="/img/dsflyer/punkt7.png" align="middle" height="40" width="40">
<b>Formate lange lesbar:</b> Die meiste Software nutzt quelloffene Formate. Selbst alte Dokumente bzw. Dateien können so noch nach vielen Jahren gelesen werden. Kennen Sie noch Corel Draw? Ein Programm mit dem vor einiger Zeit noch fast jedes Büro gearbeitet hat? Heute ist das Geschichte und die Formate sind kaum noch lesbar. Jeder, der sie konvertieren oder ein Programm dafür bauen möchte, muss erst Lizenzen beim Hersteller erwerben. Durch quelloffene Formate können heute erstelle Dateien auch noch in 10 Jahren, ganz ohne Zusatzkosten, ausgelesen werden.<br>
<img src="/img/dsflyer/punkt8.png" align="middle" height="40" width="40">
<b>Sicherheit:</b> Kein System ist zu 100% sicher! Linux bietet aber die Möglichkeit nachvollziehbar das eigene System abzusichern, man muss sich hierbei nicht auf Versprechen aus der Blackbox verlassen. Einige Unternehmen setzen, z. B. serverseitig, sehr stark auf Linux. Niederländische, spanische, italienische und kanadische Behörden sind bereits auf Linux umgestiegen.<br>
<img src="/img/dsflyer/punkt9.png" align="middle" height="40" width="40">
<b>Datensparsam:</b> Im Gegensatz zu anderen Betriebssystemen werden bei den meisten Linux Distributionen, nur wenige bis keine Daten an Entwickler und Dritte übermittelt. Ein Beispiel ist der Versand von Crash Reports und die Analyse von Downloads. Der Umfang und die Einstellungsmöglichkeiten können variieren.<br>
<img src="/img/dsflyer/punkt10.png" align="middle" height="40" width="40">
<b>Hilfe:</b> Sie können bei Problemen eine große Community um Rat bitten. Es gibt zahlreiche Chaträume, Foren oder Hilfeseiten.<br>

&plus;  <b>Keine Zwangsupgrades;</b> wenn Sie diese nicht wollen. Updates benötigen oft keinen Neustart.<br>
&plus; <b>Bekannte Firmen setzen auf Linux:</b> Nicht nur die Community, sondern auch professionelle Firmen wie RedHat (Fedora) und Canonical (Ubuntu) entwickeln mit.

### IV. Kritik ###

Doch Linux hat nicht nur Vorteile, genau wie jedes anderes Betriebssystem.

* **Bugbehebung:** Einige Bugs bleiben teilweise jahrelang ungelöst. Dieser Kritikpunkt wird des öfteren in Diskussionen angeführt. Hierbei besteht jedoch die Möglichkeit, dass sich jemand der Sache annimmt, da der Quellcode öffentlich verfügbar und eine Beteiligung somit jederzeit möglich ist.
* **Fehlende Treiber und Kompatibilität:** Teilweise fehlen Gerätetreiber oder entsprechender Code wird zurückgehalten. Gerade bei neueren Geräten kann es zu Komplikationen kommen. Vor dem Neukauf muss man sich also über die Kompatibilität mit seiner gewünschten Distribution informieren. Für die Kompatibilität mit externer Hardware lohnt sich auch ein Blick in diesen [Ubuntuusers Artikel](https://wiki.ubuntuusers.de/Hardwaredatenbanken/).


Ebenso als negativer Punkt kann einem die geänderte Usability zu gewohnten Betriebssystem einfallen, jedoch möchten wir diesen so nicht gelten lassen. Das ist wie, wenn man einen Apfel mit einer Birne vergleicht.

### V. Schlussworte ###

Soviel zum Thema Linux für heute.
Diese Artikelserie ist für "Einsteiger" gedacht. Wir haben ein paar Dinge verallgemeinert. Natürlich kann dieser Themenbereich wesentlich komplexer sein und viel know-how benötigen, nicht umsonst sind Handbücher hunderte Seiten lang. Wir wollen jedoch potenzielle Interessenten nicht langweilen oder überfordern, sondern leicht verständlich an das Thema heranführen. In den nächsten Artikeln könnt ihr euch auf detailliertere Erklärungen, Hinweise zu den verschiedenen Distributionen, Installationshilfen sowie nützlichen Programmen freuen.

Die Gründe für Linux gibt es auch als Flyer. Vielen Dank an Messtome für die Unterstützung und Geduld bei der Gestaltung!


<div style="text-align: center">  <a href="/files/Flyer_Linux_final_Webseite.pdf" class="btn btn-small btn-template-main">Download Flyer</a></div>

<br><br>

Wir würden uns auf euer Feedback zu diesem Beitrag freuen. :)

> Lizenzhinweis zum Thumbnail: Der Tux Pinguin wurde erstellt von Larry Ewing (lewing@isc.tamu.edu) and The GIMP
