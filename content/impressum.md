+++
title = "Impressum"
+++
<h3>Verantwortlich im Sinne des § 5 TMG:</h3>
<b>Anoxinon e.V.</b><br>
<p>c/o Christopher Bodtke<br>
Jahnstraße 3, 14513 Teltow<br>
Deutschland<br><br>
Amtsgericht Potsdam<br>
Registernummer: 9009<br><br>
Vertretungsberechtigter Vorstand: Christopher Bodtke, Michael Gulden, Malte Kiefer, Patrick Heinrich von den Driesch<br>
</p><br>
<h4>Inhaltlich Verantwortlicher gemäß § 55 Abs. 2 Rundfunkstaatsvertrag (RStV):</h4>
Christopher Bodtke<br>
c/o Anoxinon e.V.<br>
Jahnstraße 3, 14513 Teltow<br>
Deutschland<br>
<h3>Kontakt:</h3>
<p>
Telefon: (+49) 157 / 322 394 90<br>
E-Mail: postfach[ett]anoxinon(dot)de
</p>
<h3>Lizenzhinweis Design</h3>
Original Template von <a href="http://bootstrapious.com/free-templates">Bootstrapious</a>, angepasst für Hugo durch <a href="https://github.com/devcows/hugo-universal-theme">DevCows</a>, Eigene Änderungen wurden vorgenommen.
<br>
<h3>Adressverarbeitung</h3>
<p>
Alle die auf dieser Webseite angegebenen Kontaktinformationen von Personen,<br>
inklusive etwaiger Fotos dienen ausdrücklich nur zu Informationszwecken bzw.<br>
zur Kontaktaufnahme. Sie dürfen insbesondere nicht für die Zusendung von Werbung,<br>
Spam und ähnliches genutzt werden. Einer werblichen Nutzung dieser Daten wird <br>
deshalb hiermit widersprochen. Sollten diese Informationen dennoch zu den vorstehend <br>
genannten Zwecken genutzt werden, behalten wir uns etwaige rechtliche Schritte vor.
</p>
<h3>Abmahnungsrechtlicher Hinweis</h3>
<p>
Eine Verletzung von Schutzrechten Dritter oder eine Situation, die eine Aufforderung<br>
per anwaltlicher Abmahnung motivieren könnte, entspricht nicht dem mutmaßlichen<br>
Willen der Betreiber dieser Webseiten. In jedem Fall sichern wir die sofortige Behebung<br>
der Verletzung zu. Kosten einer Abmahnung oder einer anwaltlichen Beratung können wir<br>
daher als offenkundlich mißbräuchlich nicht übernehmen.
</p>
<hr>
<h2>Haftungsausschluss (Disclaimer)</h2>
<h3>Haftung für Inhalte</h3>
<p>
Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten<br>
nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter<br>
jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu<br>
überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.<br>
Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den<br>
allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst<br>
ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden<br>
von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.
</p>
<h3>Haftung für Links</h3>
<p>
Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen<br>
Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen.<br>
Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der<br>
Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche<br>
Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.<br>
Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete<br>
Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen<br>
werden wir derartige Links umgehend entfernen.
</p>
<h3>Urheberrecht</h3>
<p>
Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen<br>
dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der<br>
Verwertung außerhalb der Grenzen des Urheberrechtes bzw. <a href="/lizenzhinweis">der erteilten Lizenz</a> bedürfen der schriftlichen Zustimmung des<br>
jeweiligen Autors bzw. Ersteller. Soweit die Inhalte auf dieser Seite nicht vom Betreiber<br>
erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter<br>
als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden,
bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir
derartige Inhalte umgehend entfernen.
</p><hr>
<br>
