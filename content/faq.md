+++
title = "FAQ"
description = "Häufige Fragen"
keywords = ["FAQ","Häufige Fragen","Fragen"]


+++
> Sollten noch Fragen auftauchen, [kontaktiere](/kontakt/) uns bitte!

### 1. Was ist Anoxinon Media? ###

Anoxinon Media ist eine Plattform des Vereins Anoxinon e.V. und erfüllt dessen Zweck die Allgemeinheit über Themen wie Datensicherheit, Verschlüsselung, Datenschutz, Kryptographie, Zensur, sowie den dazugehörigen Begleitthemen zu informieren. Wir möchten im späteren Verlauf eine breitere Palette an Informationsmaterialien anbieten. Dazu gehören sowohl Podcasts als auch audiovisuelle Inhalte und Flyer.

Derzeit bieten wir unsere Inhalte nur in Textform an, wir möchten zuerst kleine Brötchen backen. In Zukunft soll sich das aber ändern.

### 2. Wieso gibt es keine Kommentarfunktion? ###

Hinter der Website steckt kein CMS und damit können wir intern keine Kommentarfunktion mit einfacher Moderation anbieten.
Drittanbieter wie Disqus sind für uns hierbei ein No-Go. Eventuell finden wir noch eine Lösung die unseren Ansprüchen genügt. Bis dahin kann man gerne
seine Kommentare via E-Mail, Mastodon oder über den MUC abgeben. Sollten wir einmal auf Fehler aufmerksam gemacht werden vertuschen wir das nicht, sondern weisen transparent darauf hin und bessern diese aus.

### 3. Habt Ihr einen regelmäßigen Rythmus für Veröffentlichungen? ###
Derzeit streben wir einen Rythmus von zwei Beiträgen im Monat an. Wieviel wir am Ende tatsächlich veröffentlichen, <br>
hängt von der personellen, als auch der zeitlichen und inhaltlichen Komponente ab. Wir halten euch auf dem Laufenden. :)

### 4. Was bedeuten die Tags Anfänger, Fortgeschritten & Profi? ###
Wir schreiben über verschiedene Themen und hierbei kann es vorkommen, dass man ohne Vorwissen nur Bahnhof versteht. Aus diesem Grund bieten wir eine einfache Kennzeichnung an, nach der man sich die Inhalte für seine Stufe heraussuchen kann.

**Anfänger** = Personen, die erstes Interesse an den Themen zeigen oder schon ein wenig darüber wissen

**Fortgeschritten** = Personen, die bereits wichtige Schritte umgesetzt haben und ein Bewusstsein für die Thematiken entwickeln

**Profi** = Personen, die viele Schritte umgesetzt haben oder kurz davor sind.

### 5. Wie kann ich mithelfen? ###
Wir sind immer auf der Suche nach Personen die unser Team ergänzen können.<br>
Solltest Du Interesse haben mitzuwirken, schau doch einmal hier: <a href="https://anoxinon.de/mitmachen/">Stellenbörse Anoxinon e.V.</a>

### 6. Wieso sind KEINE Javascripts auf der Website eingebunden? ###
Durch Javascript ist es möglich Benutzerinteraktionen auszuwerten, Inhalte zu verändern, nachzuladen oder zu generieren. Auf dem Großteil aller Websites werden diese Bausteine eingesetzt und auch missbraucht, denn diese eignen sich hervorrangend um Nutzer zu verfolgen und deren Verhalten zu analysieren. Weiterhin kann über Javascript unbemerkt Schadsoftware ausgeführt werden.<br>
<br>
Anoxinon möchte einen möglichst leichtgewichtigen und einfachen Websiteaufbau unterstützen, weshalb wir getrost auf Javascript verzichten können.<br>

### 7. Unter welcher Lizenz stehen die Inhalte? ###
Bitte besucht unsere Seite mit dem entsprechenden <a href="/lizenzhinweis/">Lizenzhinweis</a>.
